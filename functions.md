# Functions

Functions are pretty same with C.

```kuu
<type> <function_name>(<args>) do
    # code #
end
```

## Simple Example

```kuu
cimport "stdio.h"

int square(int n) do
    return n * n;
end

int given_n = 5;
printf("%d\n", square(given_n));
```

## Example

An example program that calculates factorial.

```kuu
cimport "stdio.h"

int fact(int n) do
    mut int num = 0;

    for (mut int i = 0; i < n+1; i++) do
        num *= i;
    end

    return num;
end

int given_n = 5;
printf("%d\n", fact(given_n));
```
