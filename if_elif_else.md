# If / Elif / Else

```kuu
if (<check>) do
    # ... #
end
```

```kuu
if (<check>) do
    # ... #
end
elif (<another check>) do
    # ... #
end
```

```kuu
if (<check>) do
    # ... #
end
else do
    # ... #
end
```

## Example

```kuu
int number = 10;

if (number == 0) do
    # ... #
end
elif (number == 1) do
    # ... #
end
else do
    # ... #
end
```
