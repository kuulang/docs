# Download Compiler

## Pre-requests

- Nim Programming Language
- GCC (GNU Compiler Collection)

## Compiling

- Clone the repo and run `nim c kuu.nim`
- Add the compiled file to the PATH.

## Usage

- `./kuu c file.kuu` (Will generate human-readable C source.)
- `./kuu file.kuu` (Will generate an executable binary.)
