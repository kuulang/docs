# Hello Galaxy!

```kuu
# Simple hello world example #
cimport "stdio.h"

puts("Hello Galaxy!"); # Calling a C function #
```

or you can use `./std/stdio.kuu`

```kuu
# Simple hello world example with standart library. #
import "./std/stdio.kuu"

print_str("Hello Galaxy!");
```
