# Comments

Comments starts with `#` and finishes with `#`

## Examples

```kuu
# this is a comment #
```

```kuu
# this is a comment
and it can be multi-line #
```
