# While Loop

```kuu
while (<check>) do
    # ... #
end
```

## Example

```kuu
cimport "stdio.h"

mut int i = 10;

while (i > 0) do
    printf("%d\n", i);
    i--;
end
```
