# Using C
## Keywords
- `ctranspile`: Add a C code while transpiling.
- `cimport`: Import a C library.
- `cdef`: Add a C code to the starting.

### Examples
```kuu
cimport "stdio.h"

cdef "#define XD {"
cdef "#define LOL }"

for (mut int i = 0; i < 10; i++) XD
    printf("%d", i);
LOL
```
```kuu
cimport "stdio.h"
ctranspile "printf(~"kuu~");"
```

## Direct Using
Transpiler also adds unknown literals to C code, so you can use C anywhere.

### Example
```kuu
cimport "stdio.h"

bool thing = yes;

if (thing == yes) { # using c #
    printf("yay!");
} else do # using kuu #
    printf("aaaaa");
end
```