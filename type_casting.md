# Type Casting

- `as <type> x`
- If you need to convert to like `long int`, use C.
  - `ctranspile "(long long int)" x`

## Without Type Casting

```kuu
int x = 7, y = 3;
mut float z;

z = x / y;

printf("%f", z); # returns 2.00000 #
```

## With Type Casting

```kuu
int x = 7, y = 3;
mut float z;

z = as float x / as float y;

printf("%f", z); # returns 2.333333 #
```
