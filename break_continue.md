# Break & Continue Keywords

## Break

You can use this keyword for finish the loop.

### Example

```kuu
cimport "stdio.h"

mut int i = 0;

while (i >= 0) do
    if (i == 10) do
        break;
    end

    printf("%d\n", i);
    i++;
end
```

## Continue

You can use this keyword for skip the loop.

### Example

```kuu
cimport "stdio.h"

for (mut int i = 0; i < 100; i++) do
    if (i % 2 == 0) do # skip if even #
        continue;
    end

    printf("%d\n", i);
end
```
