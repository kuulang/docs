# Variables

## Creating a Variable

```kuu
<type> <variable_name> = <value>;
```

## Main Types

- `int`: (`45`, `546`, `69` etc...)
- `float`: (`35.2`, `5.46`, `420.69` etc...)
- `double`: 8-byte float.
- `str`: (`"Hello World"`, `"lümenn."` etc...)
- `bool`: (`yes`, `no`)
- `char`: (`'a'`, `'B'` etc...)

**NOTE:** Main types are immutable-by-default.

### Example

```kuu
int x = 10;
str hello = "World";
```

And now try change these variables.

```kuu
int x = 10;
x = 5; # you will get an error. #
```

## Mutable Variables

```kuu
mut <type> <variable_name> = <value>;
```

### Example

```kuu
mut int x = 10;
x = 5; # this is fine. #
```
