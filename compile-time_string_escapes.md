# Compile-time String Escapes

You can use compile-time string escapes for interact with transpiler.

## Escapes

- `~n`: New line
- `~r`: Override current line
- `~t`: New tab
- `~"`: Add "
- `~~`: Add ~

## Example

```kuu
ctranspile "#ifdef _WIN32~n#include <windows.h>~n#endif"
```

When the code will transpiled, It will give the following output:

```c
#ifdef _WIN32
#include <windows.h>
#endif
```
