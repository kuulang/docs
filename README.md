# Kuu - Docs
The kuu programming language documentation.

## Index
- [Download Compiler](https://gitlab.com/kuulang/docs/-/blob/main/download_compiler.md)
- [Hello Galaxy](https://gitlab.com/kuulang/docs/-/blob/main/hello_galaxy.md)
- [Variables](https://gitlab.com/kuulang/docs/-/blob/main/variables.md)
- [Functions](https://gitlab.com/kuulang/docs/-/blob/main/functions.md)
- [Comments](https://gitlab.com/kuulang/docs/-/blob/main/comments.md)
- [Compile-time String Escapes](https://gitlab.com/kuulang/docs/-/blob/main/compile-time_string_escapes.md)
- [Type Casting](https://gitlab.com/kuulang/docs/-/blob/main/type_casting.md)
- [Using C](https://gitlab.com/kuulang/docs/-/blob/main/using_c.md)
- [If, Elif, Else](https://gitlab.com/kuulang/docs/-/blob/main/if_elif_else.md)
- [For Loop](https://gitlab.com/kuulang/docs/-/blob/main/for_loop.md)
- [While Loop](https://gitlab.com/kuulang/docs/-/blob/main/while_loop.md)
- [Repeat](https://gitlab.com/kuulang/docs/-/blob/main/repeat.md)
- [Break, Continue](https://gitlab.com/kuulang/docs/-/blob/main/break_continue.md)
- [Importing From Another File](https://gitlab.com/kuulang/docs/-/blob/main/import_keyword.md)