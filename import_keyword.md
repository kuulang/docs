# Import Keyword

If you want to use another .kuu file, you can use `import` keyword.

## Example

file2.kuu

```kuu
cimport "stdio.h"

int square(int n) do
    return n * n;
end

void print_square(int n) do
    printf("%d\n", square(n));
end
```

file1.kuu

```
import "file2.kuu"

print_square(10);
```

Compile `file1.kuu` and test the code.
