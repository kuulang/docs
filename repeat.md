# Repeat

Repeat is a compile-time keyword.

```kuu
repeat <number> do
    # job #
end
```

## Example

```kuu
cimport "stdio.h"

# prints "?" five times. #
repeat 5 do
    printf("?\n");
end
```
