# For Loop

```kuu
for (<variable>; <check>; <do>) do
    # ... #
end
```

## Example

Print numbers from 0 to 10.

```kuu
cimport "stdio.h"

for (mut int i = 0; i < 10; i++) do
    printf("%d\n", i);
end
```

Same thing but reversed.

```kuu
cimport "stdio.h"

for (mut int i = 9; i >= 0; i--) do
    printf("%d\n", i);
end
```